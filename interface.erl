-module(interface).
-export([init/0]).


parse_command(Cmd) ->
    if
        Cmd == "h\n" ->
            {ok, hit};
        Cmd == "hit\n" ->
            {ok, hit};
        Cmd == "s\n" ->
            {ok, stay};
        Cmd == "stay\n" ->
            {ok, stay};
        true ->
            {error, Cmd}
    end.


format_hand(Cards) ->
    lists:flatten(lists:map(fun (X) -> io_lib:format(" ~p", [X]) end,
                            Cards)).


get_cmd({Hands, ToPlay, _}) ->
    {_,[_|DealerHand],DealerStatus} = lists:keyfind(dealer, 1, Hands),
    {_,Hand,_} = lists:keyfind(ToPlay, 1, Hands),
    Prompt = io_lib:format(
               "Dealer's Hand: *~s (~p)~nYour hand:~s~nChoose a move: (h)it (s)tay~n> ", 
               [format_hand(DealerHand), DealerStatus, format_hand(Hand)]),
    parse_command(io:get_line(Prompt)).


get_yn(Prompt) ->
    Answer = io:get_line(Prompt),
    if 
        Answer == "y\n" ->
            true;
        Answer == "Y\n" ->
            true;
        Answer == "n\n" ->
            false;
        Answer == "N\n" ->
            false;
        true ->
            get_yn(Prompt)
    end.


loop({_,_,Winner}) when Winner /= undefined ->
    io:fwrite("Winner: ~w\n", [Winner]),
    Again = get_yn("Play again? (y/n)"),
    if 
        Again ->
            init();
        true ->
            halt()
    end;
loop(GameState) ->
    case get_cmd(GameState) of
        {error, _} ->
            loop(GameState);
        {ok, Cmd} -> 
            NewGameState = rules:update_game_state(GameState, Cmd),
            loop(rules:update_game_state(NewGameState, 
                                         rules:dealer_move(NewGameState)))
    end.


init() ->
    io:fwrite("\n\n"),
    GameState = rules:initial_game_state(),
    loop(GameState).
