-module(rules).
-import(random, [uniform/1]).
-export([update_game_state/2, initial_game_state/0, dealer_move/1]).


% Programmer rules blackjack:
%  - cards are either an integer or `ace`


%% Shuffling
draw() -> element(uniform(13),
                  {2,3,4,5,6,7,8,9,10,10,10,10,ace}).


%% Calculating Hand Value
ace_value(high) ->
    11;
ace_value(low) ->
    1.


mundane_value(Card) when is_number(Card) ->
    card.


aces_value(Aces, LowAces) ->
    1 * LowAces + 11 * (Aces - LowAces).


hand_value(Hand) ->
    MundaneValue = lists:sum(lists:filter(fun(X) -> is_number(X) end, Hand)),
    Aces = length(lists:filter(fun(X) -> is_atom(X) end, Hand)),
    hand_value(MundaneValue, Aces, 0).

hand_value(Mundane, Aces, LowAces) when Aces > LowAces->
    Value = Mundane + aces_value(Aces, LowAces),
    if
        Value > 21 ->
            hand_value(Mundane, Aces, LowAces + 1);
        true ->
            Value
    end;
hand_value(Mundane, Aces, LowAces) when Aces =:= LowAces->
    Mundane + aces_value(Aces, LowAces).


is_bust(Hand) ->
    hand_value(Hand) > 21.



%% Dealer Behavior
dealer_move({Hands, dealer, _}) ->
    {dealer, DealerHand, _} = lists:keyfind(dealer, 1, Hands),
    HandValue = hand_value(DealerHand),
    if 
        HandValue < 17 ->
            hit;
        true ->
            stay
    end.


updated_player({Name, Hand, Status}, _) when Status =:= bust ->
    {Name, Hand, bust};
updated_player({Name, Hand, _}, Move) ->
    case Move of
        hit ->
            NewHand = Hand ++ [draw()],
            IsBust = is_bust(NewHand),
            NewState = if 
                           IsBust ->
                               bust;
                           not IsBust ->
                               playing
                       end,
            {Name, NewHand, NewState};
        stay ->
            {Name, Hand, stayed}
    end.


anti_compare_players({P1, H1, _}, {P2, H2, _}) ->
    %% true if second larger
    Hv1 = hand_value(H1),
    Hv2 = hand_value(H2),
    if 
        Hv1 < Hv2 ->
            true;
        Hv1 > Hv2 ->
            false;
        Hv1 == Hv2 ->
            P2 == dealer %% dealer wins ties
    end.


filter_player_status(Players, FilterStatus) ->
    lists:filter(fun({Name, Hand, Status}) ->
                         Status == FilterStatus end,
                 Players).


winner(Players) ->
    Playing = filter_player_status(Players, playing),
    Bust = filter_player_status(Players, bust),
    Stayed = filter_player_status(Players, stayed),
    winner(Playing, Bust, Stayed).

winner(Playing, Bust, Stayed) when length(Playing) > 0 -> %% still playing
    undefined;
winner([], Bust, [Stayer]) ->
    Stayer;
winner([], _, Stayed) ->
    lists:nth(1,
              lists:sort(
                fun(P1, P2) -> not anti_compare_players(P1, P2) end,
                Stayed)).


index_of(X, Xs) ->
    index_of(X, Xs, 0).

index_of(X, [Head|Rest],I) when X == Head ->
    I;
index_of(X, [Head|Rest],I) -> 
    index_of(X, Rest, I+1).


next_player(dealer) ->
    player;
next_player(player) ->
    dealer.


initial_game_state() ->
    {[{player, [draw(), draw()], playing}, {dealer, [draw(), draw()], playing}],
     player,
     undefined}.


update_game_state({Players, ToPlay, Winner}, Move) when Winner /= undefined ->
    NextToPlay = next_player(ToPlay),
    {Players, NextToPlay, Winner};
update_game_state({Players, ToPlay, Winner}, Move) ->
    Player = lists:keyfind(ToPlay, 1, Players),
    NewPlayer = updated_player(Player, Move),
    NextToPlay = next_player(ToPlay),
    NewPlayers = lists:keyreplace(ToPlay,1, Players, NewPlayer),
    {NewPlayers,
     NextToPlay,
     winner(NewPlayers)}.
