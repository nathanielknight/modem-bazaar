# Project: Modem Bazaar

A Erlang program that plays blackjack.

## Running it

From the command line, `erl` to start the Erlang shell. Then

    1 > c(interface).
    2 > c(rules).
    3 > interface:init().
